from flask import Flask
import json
import datetime
import tempfile


app = Flask(__name__)


class Water():
    def read_water(self):
        water_details = None
        with open(f'./{self.get_filename()}.json', 'r') as f:
            data = f.read()
            water_details = json.loads(data)
        return water_details

    def read_water_by_user(self, user_id):
        water_details = None
        with open(f'./{self.get_filename()}{user_id}.json', 'r') as f:
            data = f.read()
            water_details = json.loads(data)
        return water_details

    def save_water(self, water_details):
        with open(f'./{self.get_filename()}.json', 'w') as f:
            f.write(json.dumps(water_details))

    def save_water_by_user(self, water_details, user_id):
        with open(f'./{self.get_filename()}{user_id}.json', 'w') as f:
            f.write(json.dumps(water_details))
    
    def get_filename(self):
        filename = "water"
        if 'TESTING' in app.config and app.config['TESTING']:
            filename += "_test"
        return filename


water_glass = 10
water_manager = Water()


# Obtient les détails sur les quantités d'eau
@app.route('/water', methods=['GET'])
def water():
    filename = tempfile.TemporaryFile().name
    logfile = open(filename, 'a')
    logfile.write(f'getting water at {datetime.datetime.now()}')
    logfile.close()
    return water_manager.read_water()


# Ajoute de l'eau
@app.route('/add_water', methods=['POST'])
def add_water():
    filename = tempfile.TemporaryFile().name
    logfile = open(filename, 'a')
    logfile.write(f'adding water at {datetime.datetime.now()}')
    logfile.close()
    
    water = water_manager.read_water()

    if "water" not in water.keys():
        water["water"] = water_glass
    else:
        water["water"] += water_glass
    if "adding" not in water.keys():
        water["adding"] = [{'added_at': str(datetime.datetime.now()), 'quantity': water_glass}]
        water_manager.save_water(water)
    else:
        water["adding"].append({'added_at': str(datetime.datetime.now()), 'quantity': water_glass})
        water_manager.save_water(water)
    
    return water


# Ajoute de l'eau pour un utilisateur donné
@app.route('/add_water/<user_id>', methods=['POST'])
def add_water_user(user_id):
    water = water_manager.read_water_by_user(user_id)
    water["water"] += water_glass
    
    filename = tempfile.TemporaryFile().name
    logfile = open(filename, 'a')
    logfile.write(f'adding water at {datetime.datetime.now()} for user {user_id}')
    logfile.close()
    
    water_manager.save_water_by_user(water, user_id)
    return water


if __name__ == '__main__':
    app.run(debug=True)
else:
    print('using as import')
