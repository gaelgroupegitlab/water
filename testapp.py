import pytest
import json

from app import app


def reset_test_files():
    with open('./water_test.json', 'w') as f:
        f.write(json.dumps({"water": 10, "adding": [{"added_at": "2024-05-17 09:45:19.606211", "quantity": 10}]}))
    with open('./water_test1.json', 'w') as f:
        f.write(json.dumps({"water": 5, "adding": [{"added_at": "2024-05-17 09:45:19.606211", "quantity": 5}]}))


@pytest.fixture()
def defapp():
    reset_test_files()
    app.config.update({
        "TESTING": True,
    })
    yield app


@pytest.fixture()
def client(defapp):
    return defapp.test_client()


def test_get_water(client):
    response = client.get("/water")
    result = json.loads(response.data)
    assert 10 == result['water']
    reset_test_files()


def test_save_water(client):
    response = client.post("/add_water")
    result = json.loads(response.data)
    assert 20 == result['water']
    reset_test_files()


def test_save_water_user(client):
    response = client.post("/add_water/1")
    result = json.loads(response.data)
    assert 15 == result['water']
    reset_test_files()


def test_empty_file(client):
    with open('./water_test.json', 'w') as f:
        f.write(json.dumps({}))
    
    response = client.post("/add_water")
    result = json.loads(response.data)
    assert 10 == result['water']
    reset_test_files()


